import sqlalchemy as db


market_plate_models = [
    db.Column("code", db.VARCHAR(32), nullable=False, key="code"),
    db.Column("type", db.VARCHAR(32), nullable=False, key="type"),
    db.Column(
        "name",
        db.Unicode(256, collation="utf8mb4_0900_ai_ci"),
        nullable=False,
        key="name",
    ),
    db.UniqueConstraint("code", "type", "name"),
]


stock_plate_models = [
    db.Column("stockcode", db.VARCHAR(32), nullable=False, key="stockcode"),
    db.Column("platecode", db.VARCHAR(32), nullable=False, key="platecode"),
    db.UniqueConstraint("stockcode", "platecode"),
]


market_snapshot_models = [
    db.Column("code", db.VARCHAR(32), nullable=False, key="code", index=True),
    db.Column(
        "update_time",
        db.DATETIME(timezone=True),
        nullable=False,
        key="update_time",
        index=True,
    ),
    db.Column("last_price", db.Float(32), nullable=False, key="last_price"),
    db.Column("open_price", db.Float(32), nullable=False, key="open_price"),
    db.Column("high_price", db.Float(32), nullable=False, key="high_price"),
    db.Column("low_price", db.Float(32), nullable=False, key="low_price"),
    db.Column("prev_close_price", db.Float(32), nullable=True, key="prev_close_price"),
    db.Column("volume", db.BigInteger, nullable=False, key="volume", index=True),
    db.Column("turnover", db.Float(32), nullable=False, key="turnover", index=True),
    db.Column("turnover_rate", db.Float(32), nullable=True, key="turnover_rate"),
    db.Column("suspension", db.Boolean(), nullable=False, key="suspension"),
    db.Column("equity_valid", db.Boolean(), nullable=True, key="equity_valid"),
    db.Column("issued_shares", db.BigInteger, nullable=True, key="issued_shares"),
    db.Column("total_market_val", db.Float(32), nullable=True, key="total_market_val"),
    db.Column("net_asset", db.BigInteger, nullable=True, key="net_asset"),
    db.Column("net_profit", db.BigInteger, nullable=True, key="net_profit"),
    db.Column(
        "earning_per_share", db.Float(32), nullable=True, key="earning_per_share"
    ),
    db.Column(
        "outstanding_shares", db.BigInteger, nullable=True, key="outstanding_shares"
    ),
    db.Column(
        "net_asset_per_share", db.Float(32), nullable=True, key="net_asset_per_share"
    ),
    db.Column(
        "circular_market_val", db.Float(32), nullable=True, key="circular_market_val"
    ),
    db.Column("ey_ratio", db.Float(32), nullable=True, key="ey_ratio"),
    db.Column("pe_ratio", db.Float(32), nullable=True, key="pe_ratio"),
    db.Column("pb_ratio", db.Float(32), nullable=True, key="pb_ratio"),
    db.Column("pe_ttm_ratio", db.Float(32), nullable=True, key="pe_ttm_ratio"),
    db.Column("dividend_ttm", db.Float(32), nullable=True, key="dividend_ttm"),
    db.Column(
        "dividend_ratio_ttm", db.Float(32), nullable=True, key="dividend_ratio_ttm"
    ),
    db.Column("dividend_lfy", db.Float(32), nullable=True, key="dividend_lfy"),
    db.Column(
        "dividend_lfy_ratio", db.Float(32), nullable=True, key="dividend_lfy_ratio"
    ),
    db.Column("stock_owner", db.VARCHAR(64), nullable=True, key="stock_owner"),
    db.Column("plate_valid", db.Boolean(), nullable=True, key="plate_valid"),
    db.Column(
        "plate_raise_count", db.BigInteger, nullable=True, key="plate_raise_count"
    ),
    db.Column("plate_fall_count", db.BigInteger, nullable=True, key="plate_fall_count"),
    db.Column(
        "plate_equal_count", db.BigInteger, nullable=True, key="plate_equal_count"
    ),
    db.Column("index_valid", db.Boolean(), nullable=True, key="index_valid"),
    db.Column(
        "index_raise_count", db.BigInteger, nullable=True, key="index_raise_count"
    ),
    db.Column("index_fall_count", db.BigInteger, nullable=True, key="index_fall_count"),
    db.Column(
        "index_equal_count", db.BigInteger, nullable=True, key="index_equal_count"
    ),
    db.Column("lot_size", db.BigInteger, nullable=True, key="lot_size"),
    db.Column("price_spread", db.Float(32), nullable=True, key="price_spread"),
    db.Column("ask_price", db.Float(32), nullable=True, key="ask_price"),
    db.Column("bid_price", db.Float(32), nullable=True, key="bid_price"),
    db.Column("ask_vol", db.Float(32), nullable=True, key="ask_vol"),
    db.Column("bid_vol", db.Float(32), nullable=True, key="bid_vol"),
    db.Column("enable_margin", db.Boolean(), nullable=True, key="enable_margin"),
    db.Column("mortgage_ratio", db.Float(32), nullable=True, key="mortgage_ratio"),
    db.Column(
        "long_margin_initial_ratio",
        db.Float(32),
        nullable=True,
        key="long_margin_initial_ratio",
    ),
    db.Column(
        "enable_short_sell", db.Boolean(), nullable=True, key="enable_short_sell"
    ),
    db.Column("short_sell_rate", db.Float(32), nullable=True, key="short_sell_rate"),
    db.Column(
        "short_available_volume",
        db.BigInteger,
        nullable=True,
        key="short_available_volume",
    ),
    db.Column(
        "short_margin_initial_ratio",
        db.Float(32),
        nullable=True,
        key="short_margin_initial_ratio",
    ),
    db.Column("sec_status", db.VARCHAR(32), nullable=True, key="sec_status"),
    db.Column("amplitude", db.Float(32), nullable=True, key="amplitude"),
    db.Column("avg_price", db.Float(32), nullable=True, key="avg_price"),
    db.Column("bid_ask_ratio", db.Float(32), nullable=True, key="bid_ask_ratio"),
    db.Column("volume_ratio", db.Float(32), nullable=True, key="volume_ratio"),
    db.Column(
        "highest52weeks_price", db.Float(32), nullable=True, key="highest52weeks_price"
    ),
    db.Column(
        "lowest52weeks_price", db.Float(32), nullable=True, key="lowest52weeks_price"
    ),
    db.Column(
        "highest_history_price",
        db.Float(32),
        nullable=True,
        key="highest_history_price",
    ),
    db.Column(
        "lowest_history_price", db.Float(32), nullable=True, key="lowest_history_price"
    ),
    db.Column("close_price_5min", db.Float(32), nullable=True, key="close_price_5min"),
    db.UniqueConstraint("code", "update_time", "suspension"),
]
