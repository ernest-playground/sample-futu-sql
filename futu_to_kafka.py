#!/usr/bin/env python

import logging

import bson
import futu as ft
import kafka
import pandas as pd
from futu import StockQuoteHandlerBase

from utils import get_logger, KafkaAdmin, read_yaml

__version__ = "0.0.1"


class StockQuoter(StockQuoteHandlerBase):
    def __init__(self, producer, topic, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        self.producer = producer
        self.topic = topic
        self._prev_msg = None
        super().__init__(*args, **kwargs)

    def on_recv_rsp(self, resp):
        rc, body = super().on_recv_rsp(resp)
        if rc != ft.RET_OK:
            self.logger.error(body)
            return rc, ft.RET_ERROR

        try:
            body["time_key"] = pd.to_datetime(
                body["data_date"] + " " + body["data_time"]
            )
            msg_body = {
                "time_key": body["time_key"].to_list()[0],
                "price": body["last_price"].to_list()[0],
                "volume": body["volume"].to_list()[0],
                "turnover": body["turnover"].to_list()[0],
                "suspension": body["suspension"].to_list()[0],
            }
            self.logger.info("Received %s", msg_body)
        except Exception:
            self.logger.exception("Cannot create message. body: %r", body)
            return rc, ft.RET_ERROR

        if self._prev_msg == msg_body:
            self.logger.info("Skip duplicate message.")
            return ft.RET_OK, body
        self._prev_msg = msg_body

        self.producer.send(
            self.topic,
            value=bson.encode(msg_body),
            key=body["code"].to_list()[0].encode(),
        )
        self.logger.info("Sent %s", msg_body)

        return ft.RET_OK, body


def get_kafka_producer(bootstrap_servers, client_id):
    return kafka.KafkaProducer(
        bootstrap_servers=bootstrap_servers,
        client_id=client_id,
        retries=3,
        linger_ms=300,
    )


def ensure_topic_exist(bootstrap_servers, client_id, topic_name):
    kafka_client = KafkaAdmin(bootstrap_servers, client_id=client_id)
    try:
        return kafka_client.create_topic(topic_name)
    except kafka.errors.TopicAlreadyExistsError:
        pass


def test_futu_connection(ctx):
    rc, err = ctx.get_global_state()
    if rc != ft.RET_OK:
        raise Exception(err)


def main():
    logger = get_logger(__name__)
    logger.info("Started program (%s).", __version__)

    config = read_yaml("conf/futu_to_kafka.yml")

    bootstrap_servers = config["kafka"]["bootstrap_servers"]
    client_id = config["kafka"]["client_id"]
    topic_name = config["kafka"]["topic_name"]

    ensure_topic_exist(bootstrap_servers, client_id, topic_name)
    producer = get_kafka_producer(bootstrap_servers, client_id)
    logger.info(
        "Connected to %r. Topic[%s] ClientId[%s]",
        bootstrap_servers,
        topic_name,
        client_id,
    )

    opend_host = config["futu"]["opend_host"]
    opend_port = int(config["futu"]["opend_port"])

    ctx = ft.OpenQuoteContext(host=opend_host, port=opend_port)
    test_futu_connection(ctx)
    ctx.set_handler(StockQuoter(producer, topic_name))

    ctx.start()
    ctx.subscribe(["HK.HSImain"], [ft.SubType.QUOTE])


if __name__ == "__main__":
    main()
