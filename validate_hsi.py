#!/usr/bin/env python


# Script to validate completeness of HSI future data
# Sometimes, data is missing for some reasons, we will
# mark thsee dates as error, and will be excluded from backtest.

__version__ = "0.0.1"

import pandas as pd
import sqlalchemy as db

from utils import get_logger


query = """
    SELECT distinct time_key AS date, UNIX_TIMESTAMP(time_key) AS ts, volume
        FROM hkhsifuturequote
        WHERE HOUR(time_key) > 9 AND HOUR(time_key) < 16;
"""


def create_db_engine(uri, poolclass=None, pool_size=5, pool_recycle=599):
    if poolclass is None:
        poolclass = db.pool.QueuePool
    return db.create_engine(
        uri, poolclass=poolclass, pool_size=pool_size, pool_recycle=pool_recycle
    )


def main():
    logger = get_logger()

    uri = "mysql+pymysql://futu:futupass1@localhost/futu"
    db_engine = create_db_engine(uri)

    logger.info("Reading from database.")
    df = pd.read_sql(query, db_engine)

    # preprocessing
    df["tsdiff"] = df.groupby(df.date.dt.date).ts.diff()
    df["voldiff"] = df.groupby(df.date.dt.date).volume.diff()

    # filtering
    logger.info("Writing to database.")
    df[
        (df.tsdiff > 15) & (df.tsdiff != 3600) & (df.tsdiff != 3601) & (df.voldiff > 2)
    ].date.dt.date.drop_duplicates().to_sql(
        "hkhsifail", db_engine, index=False, if_exists="replace"
    )

    logger.info("Finished.")


if __name__ == "__main__":
    main()
