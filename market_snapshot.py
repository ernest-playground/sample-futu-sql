#!/usr/bin/env python

import itertools
import time

import futu as ft
import numpy as np
import pandas as pd
import schedule

import models
from utils import chunks, database, get_logger, futu_utils, read_yaml


__version__ = "0.0.1"


class Quote:
    def __init__(
        self,
        host="127.0.0.1",
        port=11111,
        db_uri=None,
        snapshot_table_name=None,
        market_plate_table_name=None,
        stock_plate_table_name=None,
        snapshot_schedule=None,
        snapshot_plates=None,
        plate_schedule=None,
        plate_market=None,
        plate_type=None,
    ):
        self.logger = get_logger("Quote")
        self.ctx = self.get_ctx(host, port)
        self._snapshot_schedule = snapshot_schedule or []
        self._snapshot_plates = snapshot_plates or []
        self._plate_schedule = plate_schedule or []

        if plate_market is None:
            self._plate_market = []
        else:
            self._plate_market = list(
                map(lambda x: futu_utils.market_code(x), plate_market)
            )

        if plate_type is None:
            self._palte_type = []
        else:
            self._plate_type = list(
                map(lambda x: futu_utils.plate_type_code(x), plate_type)
            )

        if db_uri and snapshot_table_name:
            self.db_engine = database.create_engine(db_uri)
            self.snapshot_table, _ = database.create_table(
                snapshot_table_name, self.db_engine, *models.market_snapshot_models
            )
            self.market_plate_table, _ = database.create_table(
                market_plate_table_name, self.db_engine, *models.market_plate_models
            )
            self.stock_plate_table, _ = database.create_table(
                stock_plate_table_name, self.db_engine, *models.stock_plate_models
            )

    def get_ctx(self, host, port):
        return ft.OpenQuoteContext(host, port)

    @staticmethod
    def _get_plate_code_list(df):
        return sorted(list(set(df.to_dict("list")["code"])))

    def get_market_plate(
        self, markets, plates, batch_size=10, db_engine=None, db_table=None
    ):
        df = pd.DataFrame()
        max_req = 10

        for i, market_plate in enumerate(itertools.product(markets, plates)):
            if i != 0 and (i % max_req) == 0:
                # QoS: 10req/30s
                # https://futunnopen.github.io/futu-api-doc/protocol/intro.html#get-plate-list-limit  # noqa: B950
                time.sleep(30)
            market, plate = market_plate
            rc, temp_df = self.ctx.get_plate_list(market, plate)

            if rc != 0:
                self.logger.error("Error in getting %r in %s", plate, market)
                continue

            # add plate type (e.g. CONCEPT/INDUSTRY/OTHER ...etc)
            temp_df["type"] = plate

            # rename to match model
            # https://futunnopen.github.io/futu-api-doc/api/Quote_API.html#get_plate_list  # noqa: B950
            temp_df.rename(columns={"plate_name": "name"}, inplace=True)

            if df.empty:
                df = temp_df
            else:
                df = df.append(temp_df)

            self.logger.info("Retrieved %s in %s.", plate, market)

        #: list of plate code
        plate_codes = self._get_plate_code_list(df)

        if db_engine is not None and db_table is not None:
            self.logger.info("Writing to database.")

            db_engine.execute(
                db_table.insert().prefix_with("IGNORE"), df.to_dict("records")
            )

        return plate_codes

    def get_plate_stock(self, plate_codes, db_engine=None, db_table=None):
        df = pd.DataFrame()
        max_req = 10

        for i, plate_code in enumerate(plate_codes):
            if i != 0 and (i % max_req) == 0:
                # QoS: 10req/30s
                # https://futunnopen.github.io/futu-api-doc/protocol/intro.html#get-plate-stock-limit  # noqa: B950
                time.sleep(30)
            rc, temp_df = self.ctx.get_plate_stock(plate_code)

            if rc != 0:
                self.logger.error("Error in getting %r stock list.", plate_code)
                continue

            temp_df["platecode"] = plate_code

            # rename to match model
            temp_df.rename(columns={"code": "stockcode"}, inplace=True)

            if df.empty:
                df = temp_df
            else:
                df = df.append(temp_df)

            self.logger.info(
                "Retrieved %r stock list. (%i/%i)", plate_code, i + 1, len(plate_codes)
            )

        if db_engine is not None and db_table is not None:
            self.logger.info("Writing to database.")

            db_engine.execute(
                db_table.insert().prefix_with("IGNORE"), df.to_dict("records")
            )

        return list(df.stockcode.unique())

    def get_market_snapshot(
        self, stocks, db_engine=None, db_table=None, batch_size=400
    ):
        df = pd.DataFrame()

        for stock_chunk in chunks(stocks, batch_size):
            rc, temp_df = self.ctx.get_market_snapshot(stock_chunk)

            if rc != 0:
                self.logger.error(
                    "Error in getting data for %r. %s", stock_chunk, temp_df
                )
                continue

            if df.empty:
                df = temp_df
            else:
                df = df.append(temp_df)

            self.logger.info("Retrieved data for %i stocks.", len(stock_chunk))

        if db_engine is not None and db_table is not None:
            self.logger.info("Writing %i stocks to database.", len(df))

            df.replace({"N/A": np.nan}, inplace=True)

            # FIXME: Remove this when futu fixed the issue
            if "wrt_lowe_strike_price" in df:
                df["wrt_lower_strike_price"] = df["wrt_lowe_strike_price"]

            df = df[self.snapshot_table.columns.keys()].reset_index(drop=True)

            records = df.to_dict("records")
            for row in records:
                for k, v in row.items():
                    if isinstance(v, str):
                        continue
                    if np.isnan(v):
                        row[k] = None

            self.db_engine.execute(db_table.insert().prefix_with("IGNORE"), records)

    def task_plate(self):
        plate_codes = self.get_market_plate(
            self._plate_market,
            self._plate_type,
            db_engine=self.db_engine,
            db_table=self.market_plate_table,
        )
        self.get_plate_stock(
            plate_codes, db_engine=self.db_engine, db_table=self.stock_plate_table
        )

    def task_snapshot(self):
        self.get_market_snapshot(
            self.get_plate_stock(self._snapshot_plates),
            db_engine=self.db_engine,
            db_table=self.snapshot_table,
        )

    def start(self):
        self.logger.info(
            "Scheduled to download plate data at %r everyday.", self._plate_schedule
        )

        for t in self._plate_schedule:
            schedule.every().day.at(t).do(self.task_plate)

        self.logger.info(
            "Scheduled to retrieve data for %r at %r everyday.",
            self._snapshot_plates,
            self._snapshot_schedule,
        )

        for t in self._snapshot_schedule:
            schedule.every().day.at(t).do(self.task_snapshot)

        while True:
            schedule.run_pending()
            time.sleep(30)


def main():
    logger = get_logger(__name__)
    logger.info("Started program (%s).", __version__)

    config = read_yaml("conf/market_snapshot.yml")

    qx = Quote(
        host=config["futu"]["opend_host"],
        port=int(config["futu"]["opend_port"]),
        db_uri=config["mysql"]["uri"],
        snapshot_table_name=config["mysql"]["snapshot_table_name"],
        market_plate_table_name=config["mysql"]["market_plate_table_name"],
        stock_plate_table_name=config["mysql"]["stock_plate_table_name"],
        snapshot_schedule=config["snapshot"]["schedule"],
        snapshot_plates=config["snapshot"]["plate"],
        plate_schedule=config["plate"]["schedule"],
        plate_market=config["plate"]["market"],
        plate_type=config["plate"]["type"],
    )

    qx.start()
    qx.ctx.close()


if __name__ == "__main__":
    main()
