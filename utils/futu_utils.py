from futu import Market
from futu import Plate


def market_code(x):
    """
    Returns futu market code corresponds to the market code in this project.
    """
    #: mapping on market names between futu and this project
    market_map = {"HK": Market.HK, "SH": Market.SH, "SZ": Market.SZ}

    return market_map[x]


def plate_type_code(x):
    """
    Returns futu plate type code corresponds to the plate type code in this project.
    """
    #: mapping on market type between futu and this project
    plate_type_map = {
        "ALL": Plate.ALL,
        "CONCEPT": Plate.CONCEPT,
        "INDUSTRY": Plate.INDUSTRY,
        "OTHER": Plate.OTHER,
        "REGION": Plate.REGION,
    }

    return plate_type_map[x]
