import logging


def get_logger(name="root", loglevel=logging.DEBUG):
    logger = logging.getLogger(name)
    logger.setLevel(loglevel)
    ch = logging.StreamHandler()
    ch.setLevel(loglevel)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s.%(funcName)s - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)

    for h in logger.handlers:
        logger.removeHandler(h)

    logger.addHandler(ch)

    return logger
