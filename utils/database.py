import sqlalchemy as db


def create_engine(uri, poolclass=None, pool_size=5, pool_recycle=599):
    return db.create_engine(
        uri, poolclass=poolclass, pool_size=pool_size, pool_recycle=pool_recycle
    )


def create_table(name, db_engine, *args, **kwargs):
    metadata = db.MetaData()
    table = db.Table(name, metadata, *args, **kwargs)

    table.create(db_engine, checkfirst=True)

    return table, metadata
