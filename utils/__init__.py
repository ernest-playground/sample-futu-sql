from . import database
from . import futu_utils
from .kafka_admin import KafkaAdmin
from .logging import get_logger
from .readers import read_yaml


def chunks(arr, n):
    """Yield successive n-sized chunks from arr."""
    for i in range(0, len(arr), n):
        yield arr[i : i + n]


__all__ = [chunks, database, get_logger, futu_utils, read_yaml, KafkaAdmin]
