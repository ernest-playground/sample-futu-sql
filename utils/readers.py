import yaml
from expandvars import expandvars


def read_yaml(file, expandvar=True):
    with open(file, "r", encoding="utf-8") as yaml_file:

        class EnvLoader(yaml.FullLoader):
            def parse_node(self, block=False, indentless_sequence=False):
                event = super(EnvLoader, self).parse_node(block, indentless_sequence)

                if expandvar:
                    try:
                        event.value = expandvars(event.value)
                    except AttributeError:
                        pass

                return event

        return yaml.load(yaml_file, Loader=EnvLoader)
