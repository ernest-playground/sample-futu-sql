from kafka.admin import KafkaAdminClient, NewTopic


class KafkaAdmin:
    def __init__(self, bootstrap_servers="localhost:9092", client_id=None):
        self.client = KafkaAdminClient(
            bootstrap_servers=bootstrap_servers, client_id=client_id
        )

    def create_topics(self, topics, **kwargs):
        return self.client.create_topics(new_topics=topics, **kwargs)

    def create_topic(self, name, num_partitions=1, replication_factor=1):
        return self.create_topics(
            [
                NewTopic(
                    name=name,
                    num_partitions=num_partitions,
                    replication_factor=replication_factor,
                )
            ]
        )
