.PHONY: all build help
.PHONY: all bump-dev bump-patch bump-minor bump-major bump-release

include .version

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

BUILD_DATE = $(shell date --rfc-3339=date --utc)
BUILD_TIME = $(shell date --rfc-3339=seconds --utc)

all: build

## build: build docker image
build:
	@docker build \
		--label build-date="${BUILD_TIME}" \
		--tag ft:${VERSION} \
		--compress \
		.

## bump-dev: Bump dev version (0.0.2.dev1 -> 0.0.2.dev2)
bump-dev:
	@bumpversion --allow-dirty --no-tag --no-commit --verbose prenum

## bump-patch: Bump patch version (0.0.1 -> 0.0.2.dev1)
bump-patch:
	@bumpversion --allow-dirty --no-tag --no-commit --verbose patch

## bump-minor: Bump minor version (0.0.1 -> 0.1.0.dev1)
bump-minor:
	@bumpversion --allow-dirty --no-tag --no-commit --verbose minor

## bump-major: Bump major version (0.0.1 -> 1.0.0.dev1)
bump-major:
	@bumpversion --allow-dirty --no-tag --no-commit --verbose major

## bump-release: Release a version
bump-release:
	@bumpversion --verbose pre

## help: Display this message
help: Makefile
	@echo
	@echo " Choose a command to run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo
	@echo " Configuration:"
	@echo
	@echo "  VERSION:       ${VERSION}"
