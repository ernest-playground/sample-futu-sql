#!/bin/bash

user="chan"

case "$1" in
    "futu-to-kafka")
        exec gosu ${user} /app/futu_to_kafka.py ;;
    "kafka-to-mysql")
        exec gosu ${user} /app/kafka_to_mysql.py ;;
    "market_snapshot")
        exec gosu ${user} /app/market_snapshot.py ;;
    *)
        exec "$@" ;;
esac
