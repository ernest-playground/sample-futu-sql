FROM chanpl/python:3.8.2

WORKDIR /app

COPY requirements.txt ./
RUN set -ex \
	&& pip install --upgrade pip \
	&& pip install -r requirements.txt

COPY futu_to_kafka.py kafka_to_mysql.py market_snapshot.py ./
COPY models ./models
COPY utils ./utils

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
