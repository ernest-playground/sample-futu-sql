#!/usr/bin/env python

import logging
import socket

import bson
import kafka
import sqlalchemy as db

from utils import get_logger, read_yaml

__version__ = "0.0.1"


class KafkaWriter:
    def __init__(self, db_uri, table_name, bootstrap_servers, topic, **kwargs):
        self.logger = logging.getLogger(__name__)
        self.db_engine = self.create_db_engine(db_uri)
        self.table = self.create_table(table_name)
        self.consumer = self.create_kafka_consumer(
            topic, bootstrap_servers, kwargs["client_id"], kwargs["group_id"]
        )

    def close(self):
        self.consumer.close()
        self.logger.info("Program exited.")

    def create_db_engine(self, uri, poolclass=None, pool_size=5, pool_recycle=599):
        if poolclass is None:
            poolclass = db.pool.QueuePool

        self.logger.info("Connecting to %r.", uri)

        return db.create_engine(
            uri, poolclass=poolclass, pool_size=pool_size, pool_recycle=pool_recycle
        )

    def create_table(self, name):
        metadata = db.MetaData()
        table = db.Table(
            name,
            metadata,
            db.Column(
                "time_key",
                db.DATETIME(timezone=True),
                nullable=False,
                index=True,
                key="time_key",
            ),
            db.Column("price", db.Float(32), nullable=False, key="price"),
            db.Column("volume", db.BigInteger, nullable=False, key="volume"),
            db.Column("turnover", db.Float(32), nullable=False, key="turnover"),
            db.Column("suspension", db.Boolean(), nullable=False, key="suspension"),
            db.UniqueConstraint(
                "time_key", "price", "volume", "turnover", "suspension"
            ),
        )
        table.create(self.db_engine, checkfirst=True)

        return table

    def create_kafka_consumer(self, topic, bootstrap_servers, client_id, group_id):
        self.logger.info(
            "Connecting to %r. Topic[%s] ClientId[%s] GroupId[%s]",
            bootstrap_servers,
            topic,
            client_id,
            group_id,
        )

        return kafka.KafkaConsumer(
            topic,
            bootstrap_servers=bootstrap_servers,
            client_id=client_id,
            group_id=group_id,
            auto_offset_reset="latest",
            enable_auto_commit=False,
            consumer_timeout_ms=-1,
        )

    def _transform_data(self, d):
        """
        Transforms data into desried form.

        :param dict d: Data to correct in dict.
        """
        if "last_price" in d:
            d["price"] = d["last_price"]
            del d["last_price"]

    def start(self):
        for msg in self.consumer:
            val = bson.decode(msg.value)
            self._transform_data(val)
            self.logger.info(val)

            try:
                self.db_engine.execute(self.table.insert(), val)
            except db.exc.IntegrityError:
                # TODO: dig out duplicate key error to avoid
                # false catching.
                pass

            self.consumer.commit_async()


def main():
    logger = get_logger(__name__)
    logger.info("Started program (%s).", __version__)

    config = read_yaml("conf/kafka_to_mysql.yml")

    mysql_uri = config["mysql"]["uri"]
    mysql_table_name = config["mysql"]["table_name"]
    kafka_uri = config["kafka"]["bootstrap_servers"]
    kafka_topic_name = config["kafka"]["topic_name"]
    kafka_client_id = config["kafka"]["client_id"]
    kafka_group_id = config["kafka"].get(
        "group_id", "{}-dev".format(socket.gethostname())
    )

    writer = KafkaWriter(
        mysql_uri,
        mysql_table_name,
        kafka_uri,
        kafka_topic_name,
        client_id=kafka_client_id,
        group_id=kafka_group_id,
    )

    try:
        writer.start()
    except KeyboardInterrupt:
        writer.close()


if __name__ == "__main__":
    main()
